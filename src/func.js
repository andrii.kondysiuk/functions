const getSum = (str1, str2) => {
  if (isNaN(str1) || Array.isArray(str1) || isNaN(str2) || Array.isArray(str2)) {
    return false;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (postsArr, authorName) => {
    let posts = postsArr.filter(post => post.author === authorName);
    let comments = postsArr.reduce((sum, post) => {
        return sum + (post.comments === undefined ? 0 : post.comments.filter(comment => comment.author === authorName).length)
    }, 0);
    return `Post:${posts.length},comments:${comments}`;
};

function tickets(peopleInLine) {
  let cashBox = {
    25: 0,
    50: 0,
    100: 0,
  };

  for (let i = 0; i < peopleInLine.length; i++) {
    if (peopleInLine[i] === 25) {
      cashBox[25]++;
    } else if (peopleInLine[i] === 50) {
      if (cashBox[25] !== 0) {
        cashBox[25]--;
        cashBox[50]++;
      } else return "NO";
    } else if (peopleInLine[i] === 100) {
      if (cashBox[25] !== 0 && cashBox[50] !== 0) {
        cashBox[25]--;
        cashBox[50]--;
        cashBox[100]++;
      } else if (cashBox[25] > 2) {
        cashBox[25] = cashBox[25] - 3;
        cashBox[100]++;
      } else return "NO";
    }
  }
  return "YES";
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
